+------------------------------------------------------------------------------+
| Foonster Technology                                                          |
| Copyright (c) 2004 Foonster Technology                                       |
| All rights reserved.                                                         |
+------------------------------------------------------------------------------+
|                                                                              |
| Permission is hereby granted, free of charge, to any person obtaining a copy |
| of this software and associated documentation files (the "Software"), to deal| 
| in the Software without restriction, including without limitation the rights |
| to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    |
| copies of the Software, and to permit persons to whom the Software is        |
| furnished to do so, subject to the following conditions:                     |
|                                                                              |
| The above copyright notice and this permission notice shall be included in   |
| all copies or substantial portions of the Software.                          |
|                                                                              |
| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   |
| IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     |
| FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  |
| AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       |
| LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,| 
| OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE| 
| SOFTWARE.                                                                    |
|                                                                              |
+------------------------------------------------------------------------------+
//
// VERSION: 6.53.08
//
// PURPOSE: Postman is a generic PHP processing script to the e-mail 
//          gateway that parses the results of any form and sends them to
//          the specified users. This script has many formatting and 
//          operational options, most of which can be specified within 
//          a variable file "_variables.php" each form.
//
// 	 FILES:   
//
//        _postman_6_53_04.php - main processing file.
//
//        example.php - example PHP file using the script
//                 
//        EMAIL.php - Foonster Technology Email Module - YAMM
//                 
//        recaptchalib.php - Google Captcha Module 
//                 
//        stop-file.txt - file containing all words that you 
//        want to supress from the email.
//                
//        _variables.php - file containing all the variables to handle
//        processing options and destinations.  You can have many 
//        variable files if you are running multiple forms on your site.
//
//        VARIABLES IN CONFIGURATION FILE
//
//        $cStopWords = path to file containing all words that should be 
//        removed from any form field.  These are not partial and are considered
//        word boundry limitations
//
//        $lCaptcha = Do you want the captcha logic to be used when validating
//        the form.  You will need to include the HTML captcha information 
//        in your form from the example for this to work or you will just get
//        incorrect captcha errors all day.
//
//        $aEmail = associative array containing all the values required
//        to send an email to the reciepient of the form.
//
//          to                  | single email address
//          from                | single email address - $_POST['email'] default
//          fromname            | comman name associated with from 
//          cc                  | comma delimited list of email addresses
//          bcc                 | comma delimited list of email addresses
//          subject             | character string
//          msg-html            | associative array
//                              | 'path' - file path - HTML version of email
//                              | 'character-set' - character set to use for data
//                              | 'content-type' - content type set to use for data
//
//          msg-text            
//                              | 'path' - file path - PLAINTEXT version of email
//                              | 'character-set' - character set to use for data
//                              | 'content-type' - content type set to use for data
//
//          attachments         | array( '0' => array( 'path' => '' , 'name' => '' ) )
//
//        $aAcknowledgment = associative array containing all the values required
//        to send an acknowledgement email to the end-user of the form.
//
//          from                | single email address - $_POST['email'] default
//          fromname            | comman name associated with from 
//          cc                  | comma delimited list of email addresses
//          bcc                 | comma delimited list of email addresses
//          subject             | character string
//          msg-html            | associative array
//                              | 'path' - file path - HTML version of email
//                              | 'character-set' - character set to use for data
//                              | 'content-type' - content type set to use for data
//
//          msg-text            
//                              | 'path' - file path - PLAINTEXT version of email
//                              | 'character-set' - character set to use for data
//                              | 'content-type' - content type set to use for data
//          attachments         | array( '0' => array( 'path' => '' , 'name' => '' ) )
//
//       $aRequiredFields = associative array containing all the fields
//       that are verified, the length of each field and the type of variable
//		 scrubbing required.
//
//		 $aRequiredFields = array( 
//       	'Name' => array( 
//       		'id' => 'form id associated with field',
//       		'min-length' => minimum length of value,
//       		'scrub' => 'type of scrubbing for the variable.'
//       	),
//       
//
//		 Example: 
//       $aRequiredFields = array( 
//       	'Name' => array( 
//       		'id' => 'name',
//       		'min-length' => 3,
//       		'scrub' => 'ALPHA'
//       	),
//       	'Email' => array( 
//       		'id' => 'email',
//       		'length' => 3,
//       		'scrub' => 'EMAIL'
//       	)
//       );
//
//
//		 ALPHA - Only characters from A-Z and spaces.
//		 ALPHA_NUM - Only characters from A-Z & 0-9 and spaces.
//		 SIMPLE - Only characters found on the keyboard no special characters..
//		 EMAIL - Only characters that are part of a well-formed email address.
//		 HYPERLINK - A string that has been properly formatted as a URL.
//		 WHOLE_NUM - A whole number example 1000000 valid 1,000,000 invalid
//		 FLOAT_NUM - A float point number
//		 FORMAT_NUM - A properly formatted number
//		 SQL_INJECT - Only allow characters that are valid in value SQL calls.
//		 REMOVE_SPACES - Remove all spaces from the string.
//		 REMOVE_DOUBLESPACE - Remove double space and replace with single spaces.
//		 BASIC - Only characters found on the keyboard no special characters.
//
//
// RETURN:   
//
//       This process will return one of three responses.
//
//       1.) $cError : PHP Variable containing error message.
//
//       2.) $cReturnTXT : PHP Variable containing return text.
//
//       3.) $cReturnURL : If not null, the script will attempt to send
//          			   the user to the URL provided with a GET.
//
// CHANGES:   
//
//      2012-04-22: Converted from functions.php to EMAIL module.
//                  
//      2012-05-16: Modified script to allow user to set character set and
//                  content type for each HTML or TEXT part of the message.
//
//      2012-05-19: Insert try/catch exception processing.
//                  
//      2013-01-12: removed PathtoPostman and replaced with dirname( __FILE )
//
//		2013-02-17:
//
//					Removed $__PATH variable
//
//					Change available encoding methods to support base64
//
//					Change required variables to allow for min-length and scrubbing.
//
// that's all folks
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //