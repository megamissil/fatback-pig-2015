<?php
	include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aOutput = pageGet( basename($_SERVER['REQUEST_URI']) );
	$cMetaDesc = $aOutput['description'];
	$cMetaKW = $aOutput['keywords'];
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = $aOutput['abstract'];
	$layout = 'subpage';

	$gallery = photosAlbum('15');

	include("header.php");
?>


<main class="sub-container" role="main">
	<section class="sub-content">
		<h2 class="sub-title"><?=$aOutput['title']; ?></h2>
		
		<div class="sub-msg">
			<?=$aOutput['msg']; ?>

			<ul class="gallery small-block-grid-2 medium-block-grid-3 large-block-grid-3">
				<?php 
					$iter = 0;

		 			foreach($gallery['images'] as $key => $photo) { ?>
					 	<li>
					 		<a href="<?=$photo['image'] ?>" class="strip" data-strip-group="maingallery"><img src="<?=$photo['image'] ?>" alt="" class="gallery-img" /></a>
					 	</li>
						<?php 
			 				$iter++;
			 				if($iter == 3) {
			 					$iter = 0;
			 					echo '<div style="clear:both;"></div>';
			 					echo '<p>&nbsp;</p>';
							}
					} ?>
			</ul>
		</div>
	</section>
</main>

<?php
	include("footer.php");
?>