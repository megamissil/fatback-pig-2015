<?php
	include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aOutput = pageGet( basename($_SERVER['REQUEST_URI']) );
	$cMetaDesc = $aOutput['description'];
	$cMetaKW = $aOutput['keywords'];
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = $aOutput['abstract'];
	$layout = 'subpage';

	include("header.php");
?>


<main class="sub-container" role="main">
	<section class="sub-content">
		<h2 class="sub-title"><?=$aOutput['title']; ?></h2>
		
		<div class="sub-msg">
			<?=$aOutput['msg']; ?>

			<form action="<?php $_SERVER['PHP_SELF']?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
				<div class="row">
					<label for="name">Name:</label>
					<input type="text" name="name" id="name" value="<?=$_POST['name'] ?>"/>
				</div>
				
				<div class="row">
					<label for="email">Email:</label>
					<input type="text" name="email" id="email" value="<?=$_POST['email'] ?>"/>
				</div>
				
				<div class="row">
					<label for="phone">Phone Number:</label>
					<input type="text" name="phone" id="phone"/>
				</div>	
				
				<div class="row">
					<label for="msg">Comments:</label>
					<textarea id="msg" value="<?=$_POST['msg'] ?>" name="msg"></textarea>
				</div>
				<input type="submit" class="button" value="Submit" name="sbmtbtn" id="sbmtbtn">
			</form>
		</div>
	</section>
</main>

<?php
	include("footer.php");
?>