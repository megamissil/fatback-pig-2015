	</div>
</div>

<footer id="footer">
	<div class="row show-for-medium-up">
		<div class="medium-5 large-4 columns">
			<p>Copyright <?php echo date('Y'); ?> FATBACK Pig Project</p>
		</div>
		<div class="medium-4 large-4 columns">
			<ul class="no-bullet social-links">
				<li><a href="https://www.facebook.com/pages/Fatback-Pig-Project/1406730959642922?sk=timeline" target="_blank"><img src="/img/fb-icon.png" alt="fb"></a></li>
				<li><a href="https://instagram.com/fatback_pig/" target="_blank"><img src="/img/ig-icon.png" alt="ig"></a></li>
				<li><a href="https://twitter.com/FatbackPig2" target="_blank"><img src="/img/tw-icon.png" alt="tw"></a></li>
			</ul>
		</div>
		<div class="medium-3 large-4 columns">
			<a href="https://zeekeeinteractive.com/" target="_blank"><img id="zeekee-slug" src="/img/zeekee-slug-white-2011.png" alt="zeekee"></a>
		</div>
	</div>

	<div class="show-for-small-only">
		<div class="small-12 columns">
			<ul class="no-bullet social-links">
				<li><a href="https://www.facebook.com/pages/Fatback-Pig-Project/1406730959642922?sk=timeline" target="_blank"><img src="/img/fb-icon.png" alt="fb"></a></li>
				<li><a href="https://instagram.com/fatback_pig/" target="_blank"><img src="/img/ig-icon.png" alt="ig"></a></li>
				<li><a href="https://twitter.com/FatbackPig2" target="_blank"><img src="/img/tw-icon.png" alt="tw"></a></li>
			</ul>
		</div>
		<div class="small-8 columns">
			<p>Copyright <?php echo date('Y'); ?> FATBACK Pig Project</p>
		</div>
		<div class="small-4 columns">
			<a href="https://zeekeeinteractive.com/" target="_blank"><img id="zeekee-slug" src="/img/zeekee-slug-white-2011.png" alt="zeekee"></a>
		</div>
	</div>
</footer>

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/foundation/js/foundation.min.js"></script>
<script src="/bower_components/matchHeight/jquery.matchHeight-min.js"></script>
<script src="/bower_components/strip/dist/js/strip.pkgd.min.js"></script>
<script>
	$(document).ready(function() {
	  var items = ["bg1.jpg", "bg2.jpg", "bg3.jpg", "bg4.jpg", "bg5.jpg", "bg6.jpg"];
	  
	  function getRandomImage() {
	    return items[ Math.floor( Math.random() * items.length ) ];
	  }
	  
	  $('.sidenav').css('background-image', "url('/img/bg/" + getRandomImage() + "')");
	  $('.smallnav').css('background-image', "url('/img/bg/" + getRandomImage() + "')");
	});
</script>
<script src="/js/app-lib.js"></script>
</body>
</html>
