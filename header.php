<?php
    $cSiteName = 'FATBACK Pig Project';
    $cSiteCity = '';
    $cSiteState = '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="<?php echo $cMetaDesc; ?>" name="description">
    <meta content="<?php echo $cMetaKW; ?>" name="keywords">
    <title><?php if (!empty($cSEOTitle)) { echo $cSEOTitle; } else if (!empty($cPageTitle)) { ?><?=$cSiteName; ?> – <?php echo $cPageTitle; ?><?php } else { ?><?=$cSiteName; ?> – <?=$cSiteCity; ?>, <?=$cSiteState; ?><?php } ?></title>
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    
    <!-- Stylesheets -->
    <link href="/stylesheets/app.min.css" rel="stylesheet">
    <link href="/stylesheets/zeekee-support.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/bower_components/strip/dist/css/strip.css"/>

    <!-- Fonts -->
    <script src="//use.typekit.net/dif8fnp.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet'>
    
    <script src="bower_components/modernizr/modernizr.js"></script>
    
    <!-- Analytics -->
</head>

<body class="<?=$layout ?>">
    <?php if ($layout == 'home') { ?>
        <div class="row">
            <div class="medium-12 columns logo">
                <a href="/"><img src="/img/head-logo.png" alt="logo"></a>
            </div>
        </div>
    <?php } else { ?> 
    <div class="row collapse page-container">
        <div class="medium-5 large-7 columns hide-for-small">
            <div class="nav-bg">
                <div class="sidenav"></div>
                <div class="side-nav-gradient"></div>
<!--             <div class="bg-img"><script type="text/javascript">getRandomImage(random_img_sub);</script></div> -->
            <?php /* if($aOutput['images']) {
                foreach($aOutput['images'] as $cKey=> $aDocument) { ?>                      
                    <img src="/tyfoon/site/pages/images/<?=$aDocument['photo_path']?>" id="bg-img" alt="background">
            <? } } */ ?>
                <div class="sidenav-fixed">
                    <div class="row">
                        <div class="show-for-medium-only medium-6 medium-centered columns logo">
                            <a href="/"><img src="/img/head-logo.png" alt="logo"></a>
                            <nav class="top-bar" data-topbar role="navigation">
                               <ul class="title-area">
                                  <li class="name"></li>
                                  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                               </ul>

                                <section class="top-bar-section">
                                    <ul>           
                                        <li><a href="/news.php">News</a></li>
                                        <li><a href="/photos.php">Photos</a></li>
                                        <li><a href="/partner-brands.php">Partner Brands</a></li>
                                        <li><a href="/our-distributors.php">Our Distributors</a></li>
                                        <li><a href="/about-us.php">About Us</a></li>
                                        <li><a href="/contact-us.php">Contact Us</a></li>
                                    </ul>
                                </section>
                            </nav>
                        </div>
                        <div class="show-for-large-up large-5 columns end logo">
                            <a href="/"><img src="/img/head-logo.png" alt="logo"></a>
                            <nav class="top-bar" data-topbar role="navigation">
                               <ul class="title-area">
                                  <li class="name"></li>
                                  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                               </ul>

                                <section class="top-bar-section">
                                    <ul>           
                                        <li><a href="/news.php">News</a></li>
                                        <li><a href="/photos.php">Photos</a></li>
                                        <li><a href="/partner-brands.php">Partner Brands</a></li>
                                        <li><a href="/our-distributors.php">Our Distributors</a></li>
                                        <li><a href="/about-us.php">About Us</a></li>
                                        <li><a href="/contact-us.php">Contact Us</a></li>
                                    </ul>
                                </section>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="show-for-small-only smallnav">
            <div class="row">
               <div class="small-4 small-centered columns">
                    <a href="/"><img src="/img/head-logo.png" alt="logo"></a>
               </div>
            </div>
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name"></li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <ul>           
                        <li><a href="/news.php">News</a></li>
                        <li><a href="/photos.php">Photos</a></li>
                        <li><a href="/partner-brands.php">Partner Brands</a></li>
                        <li><a href="/our-distributors.php">Our Distributors</a></li>
                        <li><a href="/about-us.php">About Us</a></li>
                        <li><a href="/contact-us.php">Contact Us</a></li>
                    </ul>
                </section>
            </nav>
        </div>

        <div class="medium-7 large-5 columns">
        
        <header>

        </header>
    <?php } ?>