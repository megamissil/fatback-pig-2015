<?php
	include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aOutput = pageGet( basename($_SERVER['REQUEST_URI']) );
	$cMetaDesc = $aOutput['description'];
	$cMetaKW = $aOutput['keywords'];
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = $aOutput['abstract'];
	$layout = 'subpage';

	$aNewsArticles = pageByCategory("NEWS","ALL", 0,  25, "PUBL_DESC");

	include("header.php");
?>


<main class="sub-container" role="main">
	<div class="sub-content">
		<h2 class="sub-title"><?=$aOutput['title']; ?></h2>
		
		<section class="sub-msg">
			<?=$aOutput['msg']; ?>

<!-- 			<ul class="no-bullet news">
				<?php /* foreach( $aNewsArticles as $aNewsArticle) { ?>
					<div class="row news-item">
						<li><a class="news-title" href="<?php echo $aNewsArticle['url']; ?>"><?php echo $aNewsArticle['title']; ?></a><br/>
							<span class="date"><?=date ( 'F, j Y', strtotime($aNewsArticle['published']) ) ;?></span>
							<p><?php echo $aNewsArticle['msg_short']; ?></p>
						</li>
					</div>
				<?php } */ ?>
			</ul> -->
		</section>
	</div>
</main>

<?php
	include("footer.php");
?>