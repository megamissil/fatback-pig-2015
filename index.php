<?php
	$cMetaDesc = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
	include("header.php");
?>


<main class="container" role="main">
	<section class="pig-nav show-for-medium-up">
		<?php
			echo file_get_contents("pig-nav.svg");
		?>
	</section>

	<section class="show-for-small-only">
		<?php
			echo file_get_contents("pig-outline.svg");
		?>

        <nav class="top-bar home-small-nav" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name"></li>
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>
            <section class="top-bar-section">
                <ul>           
                    <li><a href="/news.php">News</a></li>
                    <li><a href="/photos.php">Photos</a></li>
                    <li><a href="/partner-brands.php">Partner Brands</a></li>
                    <li><a href="/our-distributors.php">Our Distributors</a></li>
                    <li><a href="/about-us.php">About Us</a></li>
                    <li><a href="/contact-us.php">Contact Us</a></li>
                </ul>
            </section>
        </nav>
	</section>
</main>

<?php
	include("footer.php");
?>
