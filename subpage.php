<?php
	include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$cMetaDesc = $aOutput['description'];
	$cMetaKW = $aOutput['keywords'];
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = $aOutput['abstract'];
	$layout = 'subpage';

	include("header.php");
?>


<main class="sub-container" role="main">
	<section class="sub-content">
		<h2 class="sub-title"><?=$aOutput['title']; ?></h2>
		
		<div class="sub-msg">
			<?=$aOutput['msg']; ?>
		</div>
	</section>
</main>

<?php
	include("footer.php");
?>